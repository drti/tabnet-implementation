"""
    TabNet utilitaries to preprocess the data.
"""

from typing import Union, Tuple
import pandas as pd
import tensorflow as tf


def convert_data_types(data: pd.DataFrame) -> Tuple[pd.DataFrame, dict]:
    """
    Function to convert the data types.
    Args:
        data: Pandas dataframe to convert.
    Returns:
        data: Pandas dataframe converted.
        types: Dictionnary of the variables types.
    """
    types = {
        column: "float32"
        if ("float" in str(data[column].dtype))
        else "int32"
        if ("int" in str(data[column].dtype))
        else data[column].dtype
        for column in data.columns
    }

    types_converter = {
        column: "float32"
        if ("float" in str(data[column].dtype))
        else "int32"
        if ("int" in str(data[column].dtype) or data[column].dtype == "bool")
        else data[column].dtype
        for column in data.columns
    }
    data = data.astype(types_converter)

    return data, types


def get_feature(
    values: pd.DataFrame, values_type: type, dimension: int = 1
) -> Union[
    tf.python.feature_column.NumericColumn, tf.python.feature_column.EmbeddingColumn
]:
    """
    Function to get the dense representation of a feature column.
    Args:
        values: Feature column.
        values_type: Type of the features.
        dimension: An integer specifying the dimension of the embedding.
    Returns:
        representation: Dense representation of the feature column.
    """
    representation = None
    if values_type in ("float32", "int32"):
        representation = tf.feature_column.numeric_column(values.name)
    elif values_type == "O":
        representation = tf.feature_column.embedding_column(
            tf.feature_column.categorical_column_with_hash_bucket(
                values.name, hash_bucket_size=len(values.unique())
            ),
            dimension=dimension,
        )
    else:
        representation = tf.feature_column.embedding_column(
            tf.feature_column.categorical_column_with_identity(values.name, 2),
            dimension=dimension,
        )
    return representation


def df_to_dataset(
    features: pd.DataFrame,
    target: pd.Series,
    shuffle: bool = False,
    batch_size: int = 50000,
) -> tf.python.data.ops.dataset_ops.TensorSliceDataset:
    """
    Function to transform a Pandas dataframe into a PrefetchDataset.
    Args:
        features: Pandas dataframe of the features.
        target: Pandas dataframe of the targets.
        shuffle: Python boolean indicating to shuffle or not the dataset.
        batch_size: Size of the batch that will be propagated through the network.
    Returns:
        dataset: Tensor dataset of the features and the targets.
    """
    target = pd.DataFrame(target)
    target, types = convert_data_types(target)
    for column in target.columns:
        if types[column] == "O":
            target[column] = pd.Categorical(target[column]).codes.astype("int32")

    dataset = tf.data.Dataset.from_tensor_slices((dict(features.copy()), target.copy()))
    if shuffle:
        dataset = dataset.shuffle(buffer_size=len(features))
    dataset = dataset.batch(batch_size).prefetch(tf.data.experimental.AUTOTUNE)
    return dataset
