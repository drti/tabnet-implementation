"""
    Python file to read the data files.
"""

import pandas as pd

COLUMNS_NAMES = [
    "Elevation",
    "Aspect",
    "Slope",
    "Horizontal_Distance_To_Hydrology",
    "Vertical_Distance_To_Hydrology",
    "Horizontal_Distance_To_Roadways",
    "Hillshade_9am",
    "Hillshade_Noon",
    "Hillshade_3pm",
    "Horizontal_Distance_To_Fire_Points",
    "Wilderness_Area1",
    "Wilderness_Area2",
    "Wilderness_Area3",
    "Wilderness_Area4",
    "Soil_Type1",
    "Soil_Type2",
    "Soil_Type3",
    "Soil_Type4",
    "Soil_Type5",
    "Soil_Type6",
    "Soil_Type7",
    "Soil_Type8",
    "Soil_Type9",
    "Soil_Type10",
    "Soil_Type11",
    "Soil_Type12",
    "Soil_Type13",
    "Soil_Type14",
    "Soil_Type15",
    "Soil_Type16",
    "Soil_Type17",
    "Soil_Type18",
    "Soil_Type19",
    "Soil_Type20",
    "Soil_Type21",
    "Soil_Type22",
    "Soil_Type23",
    "Soil_Type24",
    "Soil_Type25",
    "Soil_Type26",
    "Soil_Type27",
    "Soil_Type28",
    "Soil_Type29",
    "Soil_Type30",
    "Soil_Type31",
    "Soil_Type32",
    "Soil_Type33",
    "Soil_Type34",
    "Soil_Type35",
    "Soil_Type36",
    "Soil_Type37",
    "Soil_Type38",
    "Soil_Type39",
    "Soil_Type40",
    "Covertype",
]

FEATURES_COLUMNS = [
    "# Position_1",
    "Position_2",
    "Position_3",
    "Position_4",
    "Position_5",
    "Position_6",
    "Position_7",
    "Velocity_1",
    "Velocity_2",
    "Velocity_3",
    "Velocity_4",
    "Velocity_5",
    "Velocity_6",
    "Velocity_7",
    "Acceleration_1",
    "Acceleration_2",
    "Acceleration_3",
    "Acceleration_4",
    "Acceleration_5",
    "Acceleration_6",
    "Acceleration_7",
]

TARGET = "Torque_1"


def read_covertype():
    """
    Function to read the covertypes files.
    Returns:
        train: Training Pandas dataframe.
        test: Testing Pandas dataframe.
    """
    train = pd.read_csv(
        "data/covertype/train_covertype.csv",
        header=None,
        index_col=False,
        names=COLUMNS_NAMES,
    )
    test = pd.read_csv(
        "data/covertype/test_covertype.csv",
        header=None,
        index_col=False,
        names=COLUMNS_NAMES,
    )

    return train, test


def read_sarcos():
    """
    Function to read the sarcos files.
    Returns:
        train: Training Pandas dataframe.
        test: Testing Pandas dataframe.
    """
    train = pd.read_csv("data/sarcos/sarcos_train.csv", header=0, index_col=False)[
        FEATURES_COLUMNS + [TARGET]
    ]
    test = pd.read_csv("data/sarcos/sarcos_test.csv", header=0, index_col=False)[
        FEATURES_COLUMNS + [TARGET]
    ]
    train = train.rename(columns={"# Position_1": "Position_1"})
    test = test.rename(columns={"# Position_1": "Position_1"})

    return train, test
