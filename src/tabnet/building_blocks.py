"""
TabNet module containing the building blocks to create a TabNet model.
"""

import tensorflow as tf
import tensorflow_addons as tfa


def glu(tensor: tf.Tensor, n_units: int = None) -> tf.Tensor:
    """
    Generalized linear unit nonlinear activation.
    Args:
        tensor: Tensor of the features information.
        n_units: Number to separate the tensor in two parts.
    Returns:
        Tensor of the features information.
    """
    if n_units is None:
        n_units = tf.shape(tensor)[-1] // 2

    return tensor[..., :n_units] * tf.nn.sigmoid(tensor[..., n_units:])


class TransformBlock(tf.keras.Model):
    """
    TransformBlock composed of a Dense and a BatchNormalization layer.
    Args:
        features : Dimensionality of the hidden representation in the Dense layer.
        momentum: Momentum in ghost batch normalization.
        block_name: Name of the block.
    """

    def __init__(
        self,
        features: int,
        momentum: float = 0.9,
        block_name: str = "",
        **kwargs,
    ):
        super(TransformBlock, self).__init__(**kwargs)

        self.transform = tf.keras.layers.Dense(
            features, use_bias=False, name=f"{block_name}_transformblock_dense"
        )

        self.batch_norm = tf.keras.layers.BatchNormalization(
            axis=-1,
            momentum=momentum,
            name=f"{block_name}_transformblock_bn",
        )

    def call(self, inputs: tf.Tensor, training: bool = None) -> tf.Tensor:
        """
        Args:
            inputs: Tensor of the features information.
            training: Python boolean indicating whether the BatchNormalization layer should behave
                in training mode or in inference mode.
                - training=True: The layer will normalize its inputs using the mean and variance
                    of the current batch of inputs.
                - training=False: The layer will normalize its inputs using the mean and variance
                    of its moving statistics, learned during training.
        Returns:
            output: Tensor of the features information.
        """
        tensor = self.transform(inputs)
        output = self.batch_norm(tensor, training=training)
        return output


class AttentiveTransformBlock(tf.keras.Model):
    """
    AttentiveTransformBlock composed of a TransformBlock for feature selection:
    Sparse feature selection thanks to prior scales obtained from previous step.
    Args:
        num_features: The number of input features (i.e the number of columns for
            tabular data assuming each feature is represented with 1 dimension).
        momentum: Momentum in ghost batch normalization.
        block_name: Name of the block.
    """

    def __init__(
        self,
        num_features: int,
        momentum: float = 0.9,
        block_name: str = "",
        **kwargs,
    ):
        super(AttentiveTransformBlock, self).__init__(**kwargs)

        self.attentive_transform = TransformBlock(
            num_features,
            momentum,
            block_name=f"attentive_{block_name}",
        )

    def call(
        self, inputs: tf.Tensor, priors: tf.Tensor, training: bool = None
    ) -> tf.Tensor:
        """
        Args:
            inputs: Tensor of the processed features from the previous step.
            priors: Tensor of the prior scales denoting how much a particular feature
                has been used previously.
            training: Python boolean indicating whether the BatchNormalization layer of the
                TransformBlock should behave in training mode or in inference mode.
        Returns:
            Tensor outputed by the sparsemax transformation of the product of
            the TransformBlock ouputed tensor and priors.
        """
        feature = self.attentive_transform(inputs, training=training)

        if priors is None:
            output = feature
        else:
            output = feature * priors

        return tfa.activations.sparsemax(output)


class SharedFeatureTransformBlock(tf.keras.Model):
    """
    SharedFeatureTransformBlock from the Feature transformer composed of the shared TransformBlocks
    across all decision steps for feature processing. Each TransformBlock is followed by a gated
    linear unit (GLU) nonlinearity eventually connected to a normalized residual connection with
    normalization. Normalization with √0.5 helps to stabilize learning by ensuring that the
    variance throughout the network does not change dramatically.
    Args:
        feature_dim (Na): Dimensionality of the hidden representation in the Dense layer of the
            TransformBlocks. Each layer first maps the representation to a
            2*feature_dim-dimensional output and half of it is used to determine the nonlinearity
            of the GLU activation where the other half is used as an input to GLU, and eventually
            feature_dim-dimensional output is transferred to the next layer.
        momentum: Momentum in ghost batch normalization.
        num_shared: Number of shared TransformBlocks.
        block_name: Name of the block.
    """

    def __init__(
        self,
        feature_dim: int,
        momentum: float = 0.9,
        num_shared: int = 2,
        block_name: str = "",
        **kwargs,
    ):
        super(SharedFeatureTransformBlock, self).__init__(**kwargs)

        self.feature_dim = feature_dim
        self.num_shared = num_shared

        self.shared_list = [
            TransformBlock(
                2 * self.feature_dim,
                momentum,
                block_name=f"{block_name}_{i+1}",
            )
            for i in range(self.num_shared)
        ]

    def call(self, inputs: tf.Tensor, training: bool = None) -> tf.Tensor:
        """
        Args:
            inputs: Tensor of the filtered features by the AttentiveTransformBlock.
            training: Python boolean indicating whether the BatchNormalization layer of the
                TransformBlocks should behave in training mode or in inference mode.
        Returns:
            output_1: Tensor of the processed features by the shared TransformBlocks of the
                Feature transformer.
        """
        output_1 = inputs

        for i in range(self.num_shared):
            output_2 = self.shared_list[i](output_1, training=training)
            if i == 0:
                output_2 = glu(output_2, self.feature_dim)
            else:
                output_2 = (glu(output_2, self.feature_dim) + output_1) * tf.math.sqrt(
                    0.5
                )
            output_1 = output_2

        return output_1


class StepFeatureTransformBlock(tf.keras.Model):
    """
    StepFeatureTransformBlock from the Feature transformer composed of the decision step dependent
    TransformBlocks for feature processing. Each TransformBlock is followed by a gated
    linear unit (GLU) nonlinearity eventually connected to a normalized residual connection with
    normalization. Normalization with √0.5 helps to stabilize learning by ensuring that the variance
    throughout the network does not change dramatically.
    Args:
        feature_dim (Na): Dimensionality of the hidden representation in the Dense layer of the
            TransformBlocks. Each layer first maps the representation to a
            2*feature_dim-dimensional output and half of it is used to determine the nonlinearity
            of the GLU activation where the other half is used as an input to GLU, and eventually
            feature_dim-dimensional output is transferred to the next layer.
        momentum: Momentum in ghost batch normalization.
        num_stepblock: Number of decision step dependent TransformBlocks.
        block_name: Name of the block.
    """

    def __init__(
        self,
        feature_dim: int,
        momentum: float = 0.9,
        num_stepblock: int = 2,
        block_name: str = "",
        **kwargs,
    ):
        super(StepFeatureTransformBlock, self).__init__(**kwargs)

        self.feature_dim = feature_dim
        self.num_stepblock = num_stepblock

        self.step_list = [
            TransformBlock(
                2 * self.feature_dim,
                momentum,
                block_name=f"step_{block_name}_{i+1}",
            )
            for i in range(self.num_stepblock)
        ]

    def call(self, inputs: tf.Tensor, training: bool = None) -> tf.Tensor:
        """
        Args:
            inputs: Tensor of the processed features by the Shared TransformBlocks of the Feature
                transformer.
            training: Python boolean indicating whether the BatchNormalization layer of the
                TransformBlocks should behave in training mode or in inference mode.
        Returns:
            output_1: Tensor of the processed features by the decision step dependent
                TransformBlocks of the Feature transformer.
        """
        output_1 = inputs

        for i in range(self.num_stepblock):
            output_2 = self.step_list[i](output_1, training=training)
            output_2 = (glu(output_2, self.feature_dim) + output_1) * tf.math.sqrt(0.5)
            output_1 = output_2

        return output_1
