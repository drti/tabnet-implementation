"""
TabNet module containing the encoder.
"""

from typing import Union, Tuple
import tensorflow as tf

from tabnet.building_blocks import (
    AttentiveTransformBlock,
    SharedFeatureTransformBlock,
    StepFeatureTransformBlock,
)


class TabNetEncoder(tf.keras.Model):
    """
    TabNetEncoder for classification or regression, composed of a Feature transformer
    (SharedFeatureTransformBlock + StepFeatureTransformBlock), an attentive transformer
    (AttentiveTransformblock) and feature masking at each decision step. A split block divides
    the processed representation into two, to be used by the attentive transformer of the
    subsequent step as well as for constructing the overall output. At each decision step,
    the feature selection mask can provide interpretable information about the model’s
    functionality, and the masks are aggregated to obtain global feature importance
    attribution. A final dense is applied at the end to obtain the output prediction.
    Args:
        num_features: The number of input features (i.e the number of columns for tabular data
            assuming each feature is represented with 1 dimension).
        num_outputs: Number of outputs for classification or regression.
        feature_columns: The Tensorflow feature columns for the dataset.
        feature_dim (Na): Dimensionality of the hidden representation in the Dense layer of the
            TransformBlocks.
        output_dim (Nd): Dimensionality of the outputs of each decision step, which is later
            mapped to the final classification or regression output.
        num_decision_steps (N_steps): Number of sequential decision steps.
        relaxation_factor (gamma): Relaxation factor that promotes the reuse of each feature at
            different decision steps. When it is 1, a feature is enforced to be used only at one
            decision step and as it increases, more flexibility is provided to use a feature at
            multiple decision steps.
        sparsity_coefficient (lambda_sparse): Strength of the sparsity regularization. Sparsity
            may provide a favorable inductive bias for convergence to higher accuracy for some
            datasets where most of the input features are redundant.
        batch_momentum: Momentum in ghost batch normalization.
        epsilon: A small number for numerical stability of the entropy calculations.
        num_shared: Number of shared TransformBlocks in the Feature transformer.
        num_stepblock: Number of decision step dependent TransformBlocks in the feature
            transformer.
    """

    def __init__(
        self,
        num_features: int,
        num_outputs: int,
        feature_columns: list = None,
        feature_dim: int = 64,
        output_dim: int = 64,
        num_decision_steps: int = 5,
        relaxation_factor: float = 1.5,
        sparsity_coefficient: float = 1e-5,
        batch_momentum: float = 0.9,
        epsilon: float = 1e-5,
        num_shared: int = 2,
        num_stepblock: int = 2,
        **kwargs,
    ):

        super(TabNetEncoder, self).__init__(**kwargs)

        num_outputs = int(num_outputs)
        feature_dim = int(feature_dim)
        output_dim = int(output_dim)
        num_decision_steps = int(num_decision_steps)
        relaxation_factor = float(relaxation_factor)
        sparsity_coefficient = float(sparsity_coefficient)
        batch_momentum = float(batch_momentum)
        epsilon = float(epsilon)
        num_shared = max(1, int(num_shared))
        num_stepblock = max(1, int(num_stepblock))

        if relaxation_factor < 0.0:
            raise ValueError("`relaxation_factor` cannot be negative !")

        if sparsity_coefficient < 0.0:
            raise ValueError("`sparsity_coefficient` cannot be negative !")

        self.num_features = num_features
        self.num_outputs = num_outputs
        self.feature_dim = feature_dim
        self.output_dim = output_dim

        self.num_decision_steps = num_decision_steps
        self.relaxation_factor = relaxation_factor
        self.sparsity_coefficient = sparsity_coefficient
        self.batch_momentum = batch_momentum
        self.epsilon = epsilon
        self.num_shared = num_shared
        self.num_stepblock = num_stepblock

        self.feature_columns = feature_columns

        if self.feature_columns is not None:
            self.input_features = tf.keras.layers.DenseFeatures(feature_columns)

            self.input_bn = tf.keras.layers.BatchNormalization(
                axis=-1, momentum=batch_momentum, name="input_bn"
            )
        else:
            self.input_features = None
            self.input_bn = None

        self.shared_feature_transform = SharedFeatureTransformBlock(
            self.feature_dim,
            self.batch_momentum,
            self.num_shared,
            block_name="shared",
        )

        self.step_feature_transform_list = [
            StepFeatureTransformBlock(
                self.feature_dim,
                self.batch_momentum,
                self.num_stepblock,
                block_name=f"{i}",
            )
            for i in range(self.num_decision_steps + 1)
        ]

        self.attentive_transform_list = [
            AttentiveTransformBlock(
                self.num_features,
                self.batch_momentum,
                block_name=f"{i}",
            )
            for i in range(self.num_decision_steps)
        ]

        self.final = tf.keras.layers.Dense(
            self.num_outputs, use_bias=False, name="final_dense"
        )

    def call(
        self,
        inputs: Union[tf.python.data.ops.dataset_ops.TensorSliceDataset, tf.Tensor],
        priors_initialization: tf.Tensor = None,
        training: bool = None,
    ) -> Tuple[tf.Tensor]:
        """
        Args:
            inputs: PrefetchDataset or Tensor of the features.
            priors_initialization: Tensor of the prior scales initialization for self-supervised
                learning denoting how much a particular feature has been used previously. Some
                features are unused for the self-supervised learning and corresponding entries
                are initialized at 0 to help model’s learning.
            training: Python boolean indicating whether the BatchNormalization layer of the
                TransformBlocks should behave in training mode or in inference mode.
        Returns:
            prediction: Overall decision output for classification or regression.
            encoded: Tensor of the features encoded representation.
            importance: Tensor of the features global importances.
            steps_importances: List of tensors of the features steps importances.
        """
        if self.input_features is not None:
            features = self.input_features(inputs)
            features = self.input_bn(features, training=True)

        else:
            features = inputs

        batch_size = tf.shape(features)[0]
        output_aggregated = tf.zeros([batch_size, self.output_dim])
        masked_features = features

        if priors_initialization is not None:
            priors = priors_initialization
        else:
            priors = tf.ones([batch_size, self.num_features])

        entropy_loss = 0
        importance = 0
        steps_importances = []

        shared = self.shared_feature_transform(masked_features, training=training)
        split = self.step_feature_transform_list[0](shared, training=training)
        encoded = split

        for i in range(1, self.num_decision_steps + 1):
            keys = self.attentive_transform_list[i - 1](
                split, priors, training=training
            )
            priors *= self.relaxation_factor - keys
            masked_features = keys * features

            entropy_loss += tf.reduce_mean(
                tf.reduce_sum(-keys * tf.math.log(keys + self.epsilon), axis=1)
            ) / (tf.cast(self.num_decision_steps - 1, tf.float32))

            shared = self.shared_feature_transform(masked_features, training=training)
            split = self.step_feature_transform_list[i](shared, training=training)

            decision_out = tf.nn.relu(split[:, : self.output_dim])

            output_aggregated += decision_out
            encoded += split

            scale_agg = tf.reduce_sum(decision_out, axis=1, keepdims=True)
            importance += keys * scale_agg
            steps_importances.append(keys)

        self.add_loss(self.sparsity_coefficient * entropy_loss)

        prediction = self.final(output_aggregated)

        return prediction, encoded, importance, steps_importances
