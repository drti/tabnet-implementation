"""
TabNet module containing the classifier.
"""

import tensorflow as tf
import pandas as pd

from tabnet.encoder import TabNetEncoder


class TabNetClassifier(tf.keras.Model):
    """
    TabNetClassifier composed of a TabNetEncoder and an activation function for classification
    problems.
    Args:
        feature_columns: The Tensorflow feature columns for the dataset.
        num_features: The number of input features (i.e the number of columns for tabular data
            assuming each feature is represented with 1 dimension).
        num_outputs: Number of outputs for classification.
        feature_dim (Na): Dimensionality of the hidden representation in the Dense layer of the
            TransformBlocks.
        output_dim (Nd): Dimensionality of the outputs of each decision step, which is later
            mapped to the final classification or regression output.
        num_decision_steps (N_steps): Number of sequential decision steps.
        relaxation_factor (gamma): Relaxation factor that promotes the reuse of each feature at
            different decision steps. When it is 1, a feature is enforced to be used only at one
            decision step and as it increases, more flexibility is provided to use a feature at
            multiple decision steps.
        sparsity_coefficient (lambda_sparse): Strength of the sparsity regularization. Sparsity
            may provide a favorable inductive bias for convergence to higher accuracy for some
            datasets where most of the input features are redundant.
        batch_momentum: Momentum in ghost batch normalization.
        epsilon: A small number for numerical stability of the entropy calculations.
        num_shared: Number of shared TransformBlocks in the Feature transformer.
        num_stepblock: Number of decision step dependent TransformBlocks in the feature
            transformer.
        pretrained_encoder: Pretrained encoder layer by self-supervised learning with
            TabNetAutoencoder.
        classifier_activation: Keras activation function for classification.
    """

    def __init__(
        self,
        feature_columns: list,
        num_features: int,
        num_outputs: int,
        feature_dim: int = 64,
        output_dim: int = 64,
        num_decision_steps: int = 5,
        relaxation_factor: float = 1.5,
        sparsity_coefficient: float = 1e-5,
        batch_momentum: float = 0.9,
        epsilon: float = 1e-5,
        num_shared: int = 2,
        num_stepblock: int = 2,
        pretrained_encoder: tf.keras.Model = None,
        classifier_activation: tf.keras.activations = tf.keras.activations.softmax,
        **kwargs
    ):

        super(TabNetClassifier, self).__init__(**kwargs)

        self.classifier_activation = classifier_activation
        self.pretrained_encoder = pretrained_encoder

        if self.pretrained_encoder is not None:
            self.input_features = tf.keras.layers.DenseFeatures(feature_columns)
            self.input_bn = tf.keras.layers.BatchNormalization(
                axis=-1, momentum=batch_momentum, name="input_bn"
            )
            self.encoder = pretrained_encoder

        else:
            self.encoder = TabNetEncoder(
                num_features=num_features,
                num_outputs=num_outputs,
                feature_columns=feature_columns,
                feature_dim=feature_dim,
                output_dim=output_dim,
                num_decision_steps=num_decision_steps,
                relaxation_factor=relaxation_factor,
                sparsity_coefficient=sparsity_coefficient,
                batch_momentum=batch_momentum,
                epsilon=epsilon,
                num_shared=num_shared,
                num_stepblock=num_stepblock,
                **kwargs
            )

    def call(
        self,
        inputs: tf.python.data.ops.dataset_ops.TensorSliceDataset,
    ) -> tf.Tensor:
        """
        Prediction function for classification problems.
        Args:
            inputs: PrefetchDataset of the features.
        Returns:
            prediction: Overall decision output for classification problems.
        """
        if isinstance(inputs, pd.DataFrame):
            inputs = dict(inputs)

        if self.pretrained_encoder is not None:
            inputs = self.input_features(inputs)
            inputs = self.input_bn(inputs, training=True)

        prediction, _, _, _ = self.encoder(inputs, training=True)

        prediction = self.classifier_activation(prediction)
        return prediction

    def transform(self, inputs) -> tf.Tensor:
        """
        Encoded representation function for classification problems.
        Args:
            inputs: Dictionnary of the features.
        Returns:
            encoded: Tensor of the features encoded representation.
        """
        if isinstance(inputs, pd.DataFrame):
            inputs = dict(inputs)

        _, encoded, _, _ = self.encoder(inputs, training=True)
        return encoded

    def explain(self, inputs) -> tf.Tensor:
        """
        Features global importances function for classification problems.
        Args:
            inputs: Dictionnary of the features.
        Returns:
            importance: Tensor of the features global importances.
        """
        if isinstance(inputs, pd.DataFrame):
            inputs = dict(inputs)

        _, _, importance, _ = self.encoder(inputs, training=True)
        return importance

    def explain_steps(self, inputs) -> list:
        """
        Features steps importances function for classification problems.
        Args:
            inputs: Dictionnary of the features.
        Returns:
            steps_importances: List of tensors of the features steps importances.
        """
        if isinstance(inputs, pd.DataFrame):
            inputs = dict(inputs)

        _, _, _, steps_importances = self.encoder(inputs, training=True)
        return steps_importances

    def summary(self, *super_args, **super_kwargs):
        """
        TabNetEncoder model summary function for classification prolems.
        """
        super(TabNetClassifier, self).summary(*super_args, **super_kwargs)
        self.encoder.summary(*super_args, **super_kwargs)
