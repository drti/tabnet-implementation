##########
# "prod" image
##########

FROM tensorflow/tensorflow:2.3.1-gpu AS base
RUN mkdir /tf; mkdir /tf/tabnet-implementation
WORKDIR /tf/
ENV PYTHONPATH="/tf/tabnet-implementation/src"
LABEL maintainer="Lecardonnel Maxime"

# Req
COPY requirements.txt ./
RUN pip install -r requirements.txt

##########
# "dev" image with tests 
##########

FROM base as dev

COPY requirements.dev.txt ./
RUN pip install -r requirements.dev.txt

##########
# "lab" image with jupyter 
##########

FROM dev as lab


# NB tweaks
USER root
RUN pip install jupyterlab
ENV JUPYTER_ENABLE_LAB=1  
ENV NB_USER=root
ENV NB_UID=0
ENV NB_GID=0
ENV SHELL=bash

COPY tf_entrypoint.sh /
RUN chmod +x /tf_entrypoint.sh
ENTRYPOINT ["/tf_entrypoint.sh"]


HEALTHCHECK CMD curl --fail localhost:8888 || exit 1
